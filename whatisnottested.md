# Redis Group

## Cluster

###### Missing command test.

* *ALL*
  * *I did not test this.*

## Connection

###### Missing command test.

### Will be needed at some point.

1.  auth

### Are not recommended in a real implementation.
#### So I don't really care to make the tests.

1.  select - This did not seem to work and I am not sure why. db1 gave db0's keys back, This also happened in redis-cli.
2.  swapdb - If select does not work then who cares about swapdb.

## Geo

###### Missing command test.

* *ALL*
  * *I did not test this.*
  * *I have no need for geo tracking.*

## Hashes

##### Missing command test.

* *None*

## HyperLogLog

* *ALL*
  * *I did not test this.*

## keys

##### Missing command test.

5.  migrate
6.  move
14.  restore
15.  sort
18.  unlink - Did not exist in the version of redis I used.
19.  wait - I did not really need this right now and its hard to test.

## Lists

##### Missing command test.

* *None*

## Pub/Sub

##### Missing command test.

* *ALL*
  * *I did not test this.*

## Scripting

##### Missing command test.

* *ALL*
  * *I did not test this.*
  * *I Don't do lua and don't need this.*

## Server

##### Missing command test.

1.  bgrewriteaof
2.  bgsave
3.  client
  * All Commands.
4.  Command has been tested but not all its sub-commands.
5.  Config
  * This should include all of the sub-commands.
6.  dbsize
7.  debug
  * This should include all of the sub-commands.
8.  flushall
9.  lastsave
10.  monitor
11.  role
12.  save
13.  shutdown
  *  save
  *  nosave
14.  slaveof
15.  slowlog
16.  sync

## Sets

##### Missing command test.

* *None*

## Sorted Sets
##### Missing command test.

* *None*

## Strings

###### Missing command test.

2.  bitfield - I'm not sure how this would be used in bash.
4.  bitpos - I'm not sure how this would be used in bash and I cannot get accurate results from the server.

## Transactions

##### Missing command test.

1.  discard
2.  exec
3.  multi
4.  unwatch
5.  watch
