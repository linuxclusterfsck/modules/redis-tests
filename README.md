# redis-tests

[redis library](gitlab.com/linuxclusterfsck/modules/redis) unit-tests.

# What is tested and not?

[What is not tested](gitlab.com/linuxclusterfsck/modules/redis-tests/whatisnottested.md).

If it is not in one of the above groups in the markdown file of what is not tested and it does not say its not been tested then its probably been tested. Refer to the files in the [redis-tests](gitlab.com/linuxclusterfsck/modules/redis-tests).

### Locations.

Here are the locations and what is inside them in the redis tests.

#### test-bin

This directory contains all the exectutable scripts used in the redis-tests.

##### redis

This is an example implementation of the redis library. Probably the quickest and worst way possible due to this being the most database connections possible.

##### run-tests

This runs each test through the redis exectutable script above and performs the transaction with the database.

#### test-conf

This contains the data on where we are going to access, By default this is set for the default redis port on the localhost.

#### test-data

This contains a bash file defining the cmd array, This is the data that is sent to the server in array form.

#### test-failures

These are things that failed, The particular test in question is probably due to how bash deals with the data types used in the test. Bash is not a low enough level of language to need full bitwise operations. Basically I could not get the data to set correctly and the test would either freeze or produce incorrect data if I could get the data to set. Thus I put it down to this is a hard thing to do in bash and I just don't want to spend ours on such a niche test.
